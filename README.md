# RateMyRamen
RateMyRamen is an Android Application that allows for easy tracking of your favorite packaged Ramen!

## Features
* Review different packaged ramen brands
* Import ramen information
    * Manually
    * Barcode
* View all reviews, or view filtered
* Save to Room Database


## API Notes
Currently set up to use trial API from barcodelookup.com, once the remaining calls are used the barcode scanning will no longer work. To update the API ApiLookup.kt should be updated.


## Contributors
Jackson Bonvissuto

Kate Borowy 

Kevin Brennan

## Used Sources:
### Logo
* Designed by https://www.instagram.com/enna.does.art/
### Barcode Scanning
* https://www.youtube.com/watch?v=A8r_sY7zJgE
### Embedded Camera Usage
* https://medium.com/@dpisoni/building-a-simple-photo-app-with-jetpack-compose-camerax-and-coroutines-part-2-camera-preview-cf1d795129f6 
### Checking Internet Connection
* https://www.geeksforgeeks.org/how-to-check-internet-connection-in-kotlin/
### Camera Intent 
* https://medium.com/codex/how-to-use-the-android-activity-result-api-for-selecting-and-taking-images-5dbcc3e6324b 