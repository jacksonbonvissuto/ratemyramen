package edu.towson.cosc435.group15.ratemyramen.model


import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity
data class Ramen(
    @PrimaryKey(autoGenerate = true)
    val id:Int,

    @ColumnInfo(name = "name")
    val name: String,

    @ColumnInfo(name = "brand")
    val brand: String,

    @ColumnInfo(name = "rating")
    var rating: Int,

    @ColumnInfo(name = "review")
    val review: String,

    @ColumnInfo(name = "image_url")
    val imageUri: String?
)
//TODO use database not memory
/*@Entity
data class Ramen(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
=======
>>>>>>> origin/master
    val name: String,

    @ColumnInfo(name = "brand")
    val brand: String,

    @ColumnInfo(name = "rating")
    var rating: Int,

    @ColumnInfo(name = "review")
    val review: String,

    @ColumnInfo(name = "image_url")
    val imageUri: String?
)*/