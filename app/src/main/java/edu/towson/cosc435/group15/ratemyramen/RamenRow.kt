package edu.towson.cosc435.group15.ratemyramen

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.combinedClickable
import android.R
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.Divider
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberAsyncImagePainter
import edu.towson.cosc435.group15.ratemyramen.model.Ramen

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun RamenRow(
    index: Int,
    ramen: Ramen,
    onDelete: (Ramen) -> Unit,
    onSelect: (Ramen) -> Unit
){

    Row(
        modifier = Modifier
            .fillMaxWidth()
            .combinedClickable(onLongClick = {onDelete(ramen)}){onSelect(ramen)},
        horizontalArrangement = Arrangement.SpaceBetween
    ) {
        Column()
        {
            Text(
                fontSize = 20.sp,
                text = ramen.name,
                color = Color.DarkGray,
                modifier = Modifier
                    .fillMaxWidth(.7f)
                    .padding(4.dp),
                maxLines = 2,
                overflow = TextOverflow.Ellipsis
            )
            Text(
                fontSize = 18.sp,
                text = "Brand: ${ramen.brand}",
                color = Color.DarkGray,
                modifier = Modifier.padding(4.dp)
            )
            Text(
                fontSize = 18.sp,
                text = "Rating: ${ramen.rating}",
                color = Color.DarkGray,
                modifier = Modifier.padding(4.dp)
            )
            Text(
                fontSize = 18.sp,
                text = "${ramen.review}",
                color = Color.DarkGray,
                modifier = Modifier.padding(4.dp)
            )
        }

            if (ramen.imageUri == "null") {
                Image(
                    painter = painterResource(id = R.drawable.ic_menu_gallery),
                    contentDescription = null,
                    modifier = Modifier.size(128.dp)
                )
            } else {
                Image(
                    painter = rememberAsyncImagePainter(ramen.imageUri),
                    contentDescription = null,
                    modifier = Modifier.size(128.dp)
                )
            }

    }

    Divider(color = Color.LightGray)
}
