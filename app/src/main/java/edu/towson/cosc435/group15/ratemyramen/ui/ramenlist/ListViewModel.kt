package edu.towson.cosc435.group15.ratemyramen.ui.ramenlist

import android.app.Application
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import edu.towson.cosc435.group15.ratemyramen.data.RamenDBRepo
import edu.towson.cosc435.group15.ratemyramen.model.Ramen
import kotlinx.coroutines.launch

class ListViewModel(app: Application): AndroidViewModel(app){
    private val _ramen: MutableState<List<Ramen>> = mutableStateOf(listOf())
    val ramen: State<List<Ramen>> = _ramen

    private val _selected: MutableState<Ramen?>
    val selected: State<Ramen?>

    private val _repo: RamenDBRepo = RamenDBRepo(app)

    init{
        viewModelScope.launch{
            _ramen.value = _repo.getRamen()
        }
        _selected = mutableStateOf(null)
        selected = _selected
    }

    fun addRamen (ramen: Ramen){
        viewModelScope.launch {
            _repo.addRamen(ramen)
            _ramen.value = _repo.getRamen()
        }
    }

    suspend fun deleteRamen(ramen: Ramen){
            _repo.deleteRamen(ramen)
            _ramen.value = _repo.getRamen()
    }

    fun selectRamen(ramen: Ramen){
        _selected.value = ramen
    }

    fun setRating(ramen: Ramen, rating: Int){
        ramen.rating = rating
    }

    fun filter(find: String){
        viewModelScope.launch{
            _ramen.value = _repo.getRamen().filter {i -> i.name.contains(find, true)
                    || i.brand.contains(find, true)
                    || i.review.contains(find, true)}
        }
    }
}
