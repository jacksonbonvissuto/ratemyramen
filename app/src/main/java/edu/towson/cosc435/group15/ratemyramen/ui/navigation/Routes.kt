package edu.towson.cosc435.group15.ratemyramen.ui.navigation

sealed class Routes(val route: String) {
    object HomeScreen : Routes ("homescreen")
    object ImportMenuScreen: Routes ("importmenuscreen")
    object ListScreen: Routes ("listscreen")
    object ManualImportScreen: Routes ("manualimportscreen")
    object ManualImageImportScreen: Routes ("manualimageimportscreen")
    object QRScanScreen: Routes ("qrscanscreen")
}