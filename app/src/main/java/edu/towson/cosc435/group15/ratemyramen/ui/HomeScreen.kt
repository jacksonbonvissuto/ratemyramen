package edu.towson.cosc435.group15.ratemyramen.ui

import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Home
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import edu.towson.cosc435.group15.ratemyramen.ui.navigation.Routes

@Composable
fun HomeScreen(navController: NavHostController) {
    Column(
        modifier = Modifier.fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        //Home Button Row
        Column(
            modifier = Modifier
                .padding(bottom = 80.dp, top = 10.dp, start = 10.dp)
                .fillMaxWidth(),

            ) {
            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.Start,
            ) {
                IconButton(onClick = {
                    navController.navigate(Routes.HomeScreen.route)
                }) {
                    Icon(
                        Icons.Filled.Home, contentDescription = "Localized description",
                        Modifier.size(50.dp),
                        tint = Color.Gray

                    )
                }
            }
        }
        //MenuColumn
        Column(

        ) {
            //ImportButton
            Column(
                modifier = Modifier
                    .padding(25.dp)
                    .fillMaxWidth(),
            ) {
                Row(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.Center,
                ) {
                    Button(
                        onClick = {
                            navController.navigate(Routes.ImportMenuScreen.route)
                        },
                        modifier = Modifier.padding(12.dp),
                        colors = ButtonDefaults.buttonColors(backgroundColor = MaterialTheme.colors.secondary)

                    ) {
                        Text(
                            text = "Add Ramen",
                            color = Color.Gray,
                            fontSize = 16.sp,
                        )
                    }
                }
            }

            //ListButton
            Column(
                modifier = Modifier
                    .padding(25.dp)
                    .fillMaxWidth(),
            ) {
                Row(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.Center,
                ) {
                    Button(
                        onClick = {
                            navController.navigate(Routes.ListScreen.route)
                        },
                        modifier = Modifier.padding(12.dp),
                        colors = ButtonDefaults.buttonColors(backgroundColor = MaterialTheme.colors.secondary)

                    ) {
                        Text(
                            text = "My Ramen",
                            color = Color.Gray,
                            fontSize = 16.sp,
                        )
                    }
                }
            }
        }
    }
}