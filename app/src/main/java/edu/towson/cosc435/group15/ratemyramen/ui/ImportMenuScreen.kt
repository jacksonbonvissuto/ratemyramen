package edu.towson.cosc435.group15.ratemyramen.ui

import androidx.compose.foundation.layout.*
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AddCircle
import androidx.compose.material.icons.filled.Home
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import edu.towson.cosc435.group15.ratemyramen.ui.navigation.Routes

@Composable
fun ImportMenuScreen(navController: NavHostController) {

    Column(
        modifier = Modifier.fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        //Home Button Row
        Column(
            modifier = Modifier
                .padding(bottom = 80.dp, top = 10.dp, start = 10.dp)
                .fillMaxWidth(),

            ) {
            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.Start,
            ) {
                IconButton(onClick = {
                    navController.navigate(Routes.HomeScreen.route)
                }) {
                    Icon(
                        Icons.Filled.Home, contentDescription = "Localized description",
                        Modifier.size(50.dp),
                        tint = Color.Gray
                    )
                }
            }
        }
        //MenuColumn
        Column(

        ) {
            Row(modifier = Modifier.fillMaxWidth().padding(12.dp),
                horizontalArrangement = Arrangement.Center)
            {
                Text(
                text="Select import method",
                color = MaterialTheme.colors.primary,
                fontSize = 22.sp)}
            //BarcodeImportButton
            Column(
                modifier = Modifier
                    .padding(25.dp)
                    .fillMaxWidth(),
            ) {

                Row(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.Start,
                ) {
                    IconButton(onClick = {
                        navController.navigate(Routes.QRScanScreen.route)
                    }) {
                        Icon(
                            Icons.Filled.AddCircle, contentDescription = "Localized description",
                            Modifier.size(45.dp),
                            tint = Color(0xFF7E7F9A)
                        )
                    }
                    Text(
                        text = "Barcode Import",
                        color = MaterialTheme.colors.primary,
                        fontSize = 16.sp,
                        modifier = Modifier.padding(12.dp)
                    )
                }
            }

            //ManualImportButton
            Column(
                modifier = Modifier
                    .padding(25.dp)
                    .fillMaxWidth(),
            ) {
                Row(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.Start,
                ) {
                    IconButton(onClick = {navController.navigate(Routes.ManualImportScreen.route){
                        popUpTo(Routes.ManualImportScreen.route)
                        }
                    }
                    ) {
                        Icon(
                            Icons.Filled.AddCircle, contentDescription = "Localized description",
                            Modifier.size(45.dp),
                            tint = Color(0xFF7E7F9A),

                        )
                    }
                    Text(
                        text = "Manual Import",
                        color = MaterialTheme.colors.primary,
                        fontSize = 16.sp,
                        modifier = Modifier.padding(12.dp)
                    )
                }
            }
        }
    }
}