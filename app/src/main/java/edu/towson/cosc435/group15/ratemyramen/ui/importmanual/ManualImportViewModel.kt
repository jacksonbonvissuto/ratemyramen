package edu.towson.cosc435.group15.ratemyramen.ui.importmanual



import android.net.Uri
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import android.util.Log
import androidx.core.net.toUri
import androidx.lifecycle.ViewModel
import edu.towson.cosc435.group15.ratemyramen.model.Ramen
import edu.towson.cosc435.group15.ratemyramen.model.RamenApiObject


class ManualImportViewModel: ViewModel() {
    private val _name: MutableState<String> = mutableStateOf("")
    val name: State<String> = _name

    private val _brand: MutableState<String> = mutableStateOf("")
    val brand: State<String> = _brand

    private val _rating: MutableState<String> = mutableStateOf("")
    val rating: State<String> = _rating

    private val _review: MutableState<String> = mutableStateOf("")
    val review: State<String> = _review

    private val _imageUri: MutableState<Uri?> = mutableStateOf(null)
    val imageUri: State<Uri?> = _imageUri

    fun initName(name: String){
        _name.value = name
    }
    fun initBrand(brand: String){
        _brand.value  = brand
    }
    fun initRating(rating: String){
        _rating.value = rating
    }
    fun initReview(review: String){
        _review.value = review
    }
    fun setImageUri (uri: Uri?){
        _imageUri.value = uri
    }

    fun qrInit (apiObject: RamenApiObject){
        val testUri = apiObject.imageUrl.toUri()
        Log.d("TAG", "imageUrl: $testUri")
        initName(apiObject.title)
        initBrand(apiObject.brand)
        setImageUri(apiObject.imageUrl.toUri())
    }


    fun validateInput(): Ramen {
        if(name.value.isEmpty()){
            throw Exception("Please enter name of Ramen")
        }
        if(brand.value.isEmpty()){
            throw Exception("Please enter brand")
        }
        if(rating.value.isEmpty()){
            throw Exception("Please rate the ramen")
        }

        val rating = rating.value.toInt()
        val image = imageUri.value.toString()

        return Ramen(0, name.value, brand.value, rating, review.value, image )

    }
}