package edu.towson.cosc435.group15.ratemyramen.ui.importmanual

import android.annotation.SuppressLint
import android.widget.Toast
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import coil.compose.rememberAsyncImagePainter
import edu.towson.cosc435.group15.ratemyramen.R
import edu.towson.cosc435.group15.ratemyramen.model.Ramen
import edu.towson.cosc435.group15.ratemyramen.ui.navigation.Routes
import edu.towson.cosc435.group15.ratemyramen.ui.ramenlist.Filter

@SuppressLint("UnrememberedMutableState")
@ExperimentalComposeUiApi
@Composable
fun ManualImportScreen (navController: NavHostController,
                        vm: ManualImportViewModel = viewModel(),
                        setUri:() -> Unit,
                        onAddRamen: (Ramen) -> Unit) {
    val context = LocalContext.current
    val(textName, textBrand, textRating, textReview) = remember {FocusRequester.createRefs()}
    val keyboardController = LocalSoftwareKeyboardController.current
    //val selectedRating = mutableStateOf(Rating.Three)
    //val selectedRating = mutableStateOf("")

    LaunchedEffect(true){textName.requestFocus()}
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.padding(14.dp)
    ){
        Text("Add New Ramen Entry",
            modifier = Modifier
                .padding(20.dp)
                .focusRequester(textName),
            fontSize = 24.sp,
            color = MaterialTheme.colors.primary
        )

        OutlinedTextField(
            value = vm.name.value,
            onValueChange = vm::initName,
            placeholder = { Text("Ramen Name") },
            label = { Text("Ramen") },
            singleLine = true,
            modifier = Modifier
                .padding(12.dp)
                .focusRequester(textName),
            keyboardActions = KeyboardActions(
                onNext = {
                    //vm.initName(name)
                    textBrand.requestFocus()
                }
            ),
            keyboardOptions = KeyboardOptions(
                imeAction = ImeAction.Next,
                keyboardType = KeyboardType.Text
            )
        )
        OutlinedTextField(
            value = vm.brand.value,
            onValueChange = vm::initBrand,
            placeholder = { Text("Ramen Brand") },
            label = { Text("Brand") },
            singleLine = true,
            modifier = Modifier
                .padding(12.dp)
                .focusRequester(textBrand),
            keyboardActions = KeyboardActions(
                /*onNext = {
                    //vm.initBrand(brand)
                    textRating.requestFocus()
                }*/
                onDone = {
                    keyboardController?.hide()
                }
            ),
            keyboardOptions = KeyboardOptions(
                imeAction = ImeAction.Next,
                keyboardType = KeyboardType.Text
            )
        )


//        Row(
//            modifier = Modifier.fillMaxWidth(),
//            horizontalArrangement = Arrangement.Center,
//            verticalAlignment = Alignment.CenterVertically,
//        ) {
//            RadioButton(
//                modifier = Modifier.padding(3.dp),
//                selected = vm.rating.value == "1",//selectedRating.value == "1",
//                onClick = {
//                    vm::initRating
//                }
//            )
//            //todo: implement getByRating() db query
//            Text(
//                text = "1",
//                modifier = Modifier.padding(end = 15.dp),
//                color = MaterialTheme.colors.primary
//            )
//
//            RadioButton(
//                modifier = Modifier.padding(3.dp),
//                selected = vm.rating.value ==  "2",//selectedRating.value == "2",
//                onClick = {
//                    //selectedRating.value = "2"
//                    vm::initRating
//                }
//            )
//            Text(
//                text = "2",
//                modifier = Modifier.padding(end = 15.dp),
//                color = MaterialTheme.colors.primary
//            )
//
//            RadioButton(
//                modifier = Modifier.padding(3.dp),
//                selected = vm.rating.value == "3",
//                onClick = {
//                    //selectedRating.value = "3"
//                    vm::initRating
//                }
//            )
//            Text(
//                text = "3",
//                modifier = Modifier.padding(end = 15.dp),
//                color = MaterialTheme.colors.primary
//            )
//
//            RadioButton(
//                modifier = Modifier.padding(3.dp),
//                selected = vm.rating.value == "4",
//                onClick = {
//                    //electedRating.value = "4"
//                    vm::initRating
//                }
//            )
//            Text(
//                text = "4",
//                modifier = Modifier.padding(end = 15.dp),
//                color = MaterialTheme.colors.primary
//            )
//
//            RadioButton(
//                modifier = Modifier.padding(3.dp),
//                selected = vm.rating.value == "5",
//                onClick = {
//                    //selectedRating.value = "5"
//                    vm::initRating
//                }
//            )
//            Text(
//                text = "5",
//                modifier = Modifier.padding(end = 15.dp),
//                color = MaterialTheme.colors.primary
//            )
//        }

       OutlinedTextField(
            value = vm.rating.value,
            onValueChange = vm::initRating,
            placeholder = { Text("Rating") },
            label = { Text("Rating") },
            singleLine = true,
            modifier = Modifier
                .padding(12.dp)
                .focusRequester(textRating),
            keyboardActions = KeyboardActions(
                onNext = {
                    textReview.requestFocus()
                }
            ),
            keyboardOptions = KeyboardOptions(
                imeAction = ImeAction.Next,
                keyboardType = KeyboardType.Number
            )
        )

        OutlinedTextField(
            value = vm.review.value,
            onValueChange = vm::initReview,
            placeholder = { Text("Review") },
            label = { Text("Review") },
            //singleLine = true,
            modifier = Modifier
                .padding(12.dp)
                .focusRequester(textReview),
            keyboardActions = KeyboardActions(
                onDone = {
                    keyboardController?.hide()
                }
            ),
            keyboardOptions = KeyboardOptions(
                imeAction = ImeAction.Done,
                keyboardType = KeyboardType.Text
            )
        )

        Row(
            verticalAlignment =Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceEvenly,
            modifier = Modifier
                .padding(10.dp)
                .fillMaxWidth()
        ) {
            Button(
                onClick = { navController.navigate(Routes.ManualImageImportScreen.route) },
                shape = CircleShape,
                colors = ButtonDefaults.buttonColors(backgroundColor = MaterialTheme.colors.secondary)

            ) {
                Text("Add Image")
            }

            if (vm.imageUri.value == null){
                Image(
                    painter = painterResource(id = android.R.drawable.ic_menu_gallery),
                    contentDescription = null,
                    modifier = Modifier.size(128.dp)
                )
            }else {
                Image(
                    painter = rememberAsyncImagePainter(vm.imageUri.value),
                    contentDescription = null,
                    modifier = Modifier.size(128.dp)
                )
            }
        }

        Button(
            colors = ButtonDefaults.buttonColors(backgroundColor = MaterialTheme.colors.secondary),
            onClick = {
                try {
                    //selectedRating
                    val ramen = vm.validateInput()
                    onAddRamen(ramen)
                    vm.initName("")
                    vm.initBrand("")
                    vm.initRating("")
                    vm.initReview("")
                    vm.setImageUri(null)
                    setUri()
                }
                catch(e: Exception){
                    Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show()
                }
            }
        ){
            Text(text = "Create Rating", color = Color.Gray)
        }
    }
}

//TODO add rating via drop down or radial
//enum class Rating{ One, Two, Three, Four, Five, Empty}
//@Composable
//fun ExpandedDropDownMenuBox(expanded: Boolean, onExpandedChange: () -> Boolean, function: () -> Unit) {
//
//}
//fun rate(rating: Rating){
//    return val rate = when(rating){
//        Rating.Five ->
//        Rating.Four -> {}
//        Rating.Three ->{}
//        Rating.Two ->{}
//        Rating.One ->{}
//        //Rating.Empty ->
//    }
//}

/* ExpandedDropDownMenuBox(expanded = expanded, onExpandedChange = {expanded != expanded}){
     OutlinedTextField(
         value = vm.rating.value,
         //value = selectedText,
         onValueChange = {vm::initRating},
         modifier = Modifier
             .fillMaxWidth()
             .padding(20.dp)
             *//*.onGloballyPositioned { coordinates ->
                        //This value is used to assign to the DropDown the same width
                        textfieldSize = coordinates.size.toSize()
                    }*//*,
                label = {Text("Label")},
                singleLine = true,
                keyboardActions = KeyboardActions(
                   *//* onNext = {
                        textReview.requestFocus()
                    }*//*
                    onDone = {
                        keyboardController?.hide()
                    }
                ),
                keyboardOptions = KeyboardOptions(
                    imeAction = ImeAction.Next,
                    keyboardType = KeyboardType.Number),
                trailingIcon = {
                    Icon(
                        Icons.Filled.ArrowDropDown,"contentDescription",
                        Modifier.clickable { expanded = !expanded })
                },

            )
            DropdownMenu(
                expanded = expanded,
                onDismissRequest = { expanded = false },

                modifier = Modifier
                    .padding(20.dp)
            ) {
                ratingValue.forEach { rating ->
                    DropdownMenuItem(
                        onClick = {
                            selectedIndex = rating
                        }
                    ) {
                        Text(text = rating)
                    }
                }
            }
        }
*/